const express = require("express");

const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// [SECTION] Routes
app.get("/",(req,res) =>{
	res.send("Hello World");
})
app.get("/home",(req,res) =>{
	res.send("Hello world");
})
app.post("/hello", (req,res)=>{
	console.log(req.body);
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})
let users =[];
/*

            SCENARIO:

                We want to create a simple Users database that will perform CRUD operations based on the client request. The following routes should peform its functionality:

                

                2. "/users" route
                    - This endpoint will be used to view all the users registered in our database.

                3. "/change-password" route
                    - This will allow a registered user to update his/her password.
                    - Make sure that the user is registered in the database before applying the changes.
                        - If the user's username is found in the database, change the user's password and send a response of "User <username>'s password has been updated."
                        - If the username is not found in the database, send a response of "User does not exist."

                4. "/delete-user" route
                    - This endpoint will delete a user from the mock database, upon sending a "username" as a request.
                    - Before performing any actions in this endpoint, Make sure that the database is not empty and the user to be deleted exist.
                    - If the mock database is not empty, create a condition that will check if the user exist in the database and will peform the following actions:
                        - If the username is found in the database, remove the user and send a response "User <username> has been deleted.".
                        - If the user is not found in the database, send a response "User doesn't exists".
                    - if the mock database is empty, send a response "The user database is empty!"

                5. "/home" route
                    - This will send a response "Welcome to the homepage" upon accessing by the client.

        */


/*
1. "/signup" route
    - This will allow a client to register in our database using a username and password.
    - Make sure that the client entered the complete information before saving it in the mock database.
        - If the client entered a complete information, store it in our mock database and send a response of "User <username> successfully registered!".
            Ex. User johndoe successfully registered!
        - If the client entered a incomplete information, send a response "Please input BOTH Username and Password."
*/
app.post("/signup",(req,res)=>{
	if(req.body.userName != "" && req.body.password !=""){
		users.push(req.body);
		console.log(users);
		res.send(`User ${req.body.userName} successfully registered!`);
	}
	else{
		res.send("Please input BOTH Username and Password.");
	}
})
app.get("/users",(req,res) =>{
	res.send(users);
})
app.patch("/change-password",(req,res)=>{
	let message;
	for(i=0; i<users.length;i++){
		if(users[i].userName == req.body.userName){
			users[i].password = req.body.password;
			message =`User ${req.body.userName}'s password has been updated`;
		break;
		}
		else{
			message = `User does not exist`;
		}
	}
	res.send(message);
})
app.delete("/delete-user", (req,res)=>{
	let message;
	if(users.length>0){
		for(i=0;i<users.length;i++){
			if(users[i].userName == req.body.userName){
					users.splice([i],1);
					message =`User ${req.body.userName} has been deleted.`;
				break;
				}
			else{
					message = `User does not exist`;
				}
			}
		}
	else{
		message = "Database is empty."
	}
	res.send(message);
})
app.listen(port, ()=> console.log(`Server running at port ${port}`))

